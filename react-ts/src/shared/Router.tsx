import MainPage from "../pages/MainPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";

interface AppRoute {
  title: string;
  url: string;
  page: React.FC;
}

const routes: AppRoute[] = [{ title: "Main page", url: "/", page: MainPage }];

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        {routes.map((route) => (
          <Route key={route.url} path={route.url} element={<route.page />} />
        ))}
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
