import React from "react";
import MainComponent from "../components/Main/MainComponent";

const MainPage: React.FC = () => {
  return <MainComponent />;
};
export default MainPage;
