import React from "react";
import styles from "./MainComponent.module.scss";
import classnames from "classnames/bind";

const cx = classnames.bind(styles);

const MainComponent: React.FC = () => {
  return <div className={cx(styles.main)}>Main page</div>;
};
export default MainComponent;
