from graphene import Schema

from core.schema import CoreAppQuery
from core.mutations import CoreMutation



class Query(
    CoreAppQuery,
):
    pass


class Mutation(
    CoreMutation,
):
    pass


schema = Schema(query=Query, mutation=Mutation)
