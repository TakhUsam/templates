"""backend URL Configuration
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from graphql_playground.views import GraphQLPlaygroundView
from django.conf import settings
from graphene_file_upload.django import FileUploadGraphQLView

from app.views import schema_sdl_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('graphql/', csrf_exempt(FileUploadGraphQLView.as_view(graphiql=settings.DEBUG))),
]

if settings.DEBUG:
    urlpatterns += [
        path('graphql-schema/', schema_sdl_view),
        path('playground/', GraphQLPlaygroundView.as_view(endpoint="/graphql/")),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
