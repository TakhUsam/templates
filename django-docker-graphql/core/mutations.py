import graphene
import graphql_jwt
from graphql_jwt.utils import jwt_payload, jwt_encode
from .utils.create_user import create_user
from .utils.sms import send_security_code, validate_security_code
from .types import UserType
from .models import User


class UserRegisterInputType(graphene.InputObjectType):
    phone = graphene.String(required=True)
    email = graphene.String(required=True)


class VerifySecurityCodeInputType(graphene.InputObjectType):
    phone = graphene.String(required=True)
    security_code = graphene.String(required=True)


class UserRegister(graphene.Mutation):
    """
    Mutation that receives 'phone' and 'email', creates a user (if doesn't exist)
    with provided data and send security code through sms to the provided phone number
    """
    class Arguments:
        input = UserRegisterInputType(required=True)

    user = graphene.Field(UserType)

    @classmethod
    def mutate(cls, root, info, **data):
        phone = data.get('input').get('phone')
        email = data.get('input').get('email')
        user = User.objects.filter(email=email, phone=phone).first()
        if not user:
            user = create_user(phone, email)
        send_security_code(phone)
        return UserRegister(user=user)


class CreateToken(graphene.Mutation):
    """Mutation that receives 'phone' and 'security_code'
    and returns a jwt token
    """
    class Arguments:
        input = VerifySecurityCodeInputType(required=True)

    user = graphene.Field(UserType)
    token = graphene.String()

    @classmethod
    def mutate(cls, root, info, **data):
        phone = data.get('input').get('phone')
        security_code = data.get('input').get('security_code')
        validate_security_code(phone, security_code)
        user = User.objects.get(phone=phone)
        payload = jwt_payload(user)
        token = jwt_encode(payload)
        return CreateToken(user=user, token=token)


class CoreMutation(graphene.ObjectType):
    user_register = UserRegister.Field()
    token_auth = CreateToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
