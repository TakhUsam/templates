from graphene import ObjectType, Float, Field, List, String
from graphql_jwt.decorators import login_required

from .enums import UserRole
from .types import UserType, AddressType, AddressSuggestionResultType
from .utils.geocoding import geocoder
from core.utils.guards.login_required import login_required_query
from core.utils.query_decorators.model_query_all_with_filter import model_query_all_with_filter
from .utils.guards.role_check import user_has_any_roles_query


@login_required_query
@user_has_any_roles_query([UserRole.admin])
@model_query_all_with_filter(UserType)
class UsersQuery(ObjectType):
    pass


@login_required_query
class MeQuery(ObjectType):
    me = Field(UserType)

    def resolve_me(self, info, **kwargs):
        return info.context.user


class GeocodingQuery(ObjectType):
    geocode = Field(AddressType, lat=Float(required=True), lon=Float(required=True))
    address_suggestions = Field(List(AddressSuggestionResultType), address_str=String(required=True))

    @login_required
    def resolve_geocode(self, info, **kwargs):
        return geocoder.geocode(kwargs.get('lat'), kwargs.get('lon'))

    @login_required
    def resolve_address_suggestions(self, info, **kwargs):
        return geocoder.get_address_suggestions(kwargs.get('address_str'))
