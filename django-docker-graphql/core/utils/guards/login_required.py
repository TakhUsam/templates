from graphql_jwt.decorators import login_required


def login_required_query(query):

    for attr_name in dir(query):
        if attr_name.startswith('resolve_'):
            attr = getattr(query, attr_name)
            setattr(query, attr_name, login_required(attr))

    return query
