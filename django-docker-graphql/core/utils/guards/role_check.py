from typing import List

from graphql_jwt.decorators import user_passes_test

from core.enums import UserRole


def user_has_role(role: UserRole):
    return user_has_any_roles([role])


def user_has_any_roles(roles: List[UserRole]):
    return user_passes_test(lambda u: u.role in [role.value for role in roles])


def user_has_any_roles_query(roles: List[UserRole]):
    def decorator(query_cls):
        for attr_name in dir(query_cls):
            if attr_name.startswith('resolve_'):
                resolver = getattr(query_cls, attr_name)
                setattr(query_cls, attr_name, user_has_any_roles(roles)(resolver))
        return query_cls
    return decorator
