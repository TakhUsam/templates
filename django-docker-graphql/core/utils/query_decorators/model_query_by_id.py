from graphene import Field, Argument, ID


def model_query_by_id(model_type):
    def decorator(cls):

        field = Field(model_type, id=Argument(ID, required=True))

        def resolver(self, info, **kwargs):
            return model_type._meta.model.objects.get(id=kwargs.get('id'))

        name_singular = model_type._meta.model._meta.verbose_name.replace(' ', '_')
        setattr(cls, name_singular, field)
        setattr(cls, f'resolve_{name_singular}', resolver)

        return cls

    return decorator
