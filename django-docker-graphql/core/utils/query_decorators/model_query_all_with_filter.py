from graphene_django import DjangoConnectionField
from graphene_django.filter import DjangoFilterConnectionField

from graphene.utils.str_converters import to_snake_case


def model_query_all_with_filter(model_type, model_filter=None):
    def decorator(cls):

        # generate a suitable name based on query class name
        # we don't want the word Query in the name and we want snake case
        field_name = to_snake_case(cls.__name__.replace('Query', ''))

        # query class may provide custom queryset with a specific name
        queryset_attr_name = f'get_queryset_{field_name}'

        # if no custom queryset is present use default
        if hasattr(cls, queryset_attr_name):
            get_queryset = getattr(cls, queryset_attr_name)
        else:
            def get_queryset(self, info, **kwargs):
                return model_type._meta.model.objects.all()

        # create the field with a filter if model filter is provided
        field = DjangoFilterConnectionField(model_type, filterset_class=model_filter) \
            if model_filter else DjangoConnectionField(model_type)

        def resolver(self, info, **kwargs):
            return get_queryset(self, info, **kwargs)

        # add the created field to the query class
        setattr(cls, f'{field_name}', field)
        # using setattr doesn't work when using query as a field
        cls._meta.fields[field_name] = field
        # add the resolver to the query class
        setattr(cls, f'resolve_{field_name}', resolver)

        return cls

    return decorator
