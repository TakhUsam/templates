from graphene import ObjectType


def extract_queries(module):
    classes = [getattr(module, query) for query in dir(module) if query.endswith('Query')]
    return tuple(cls for cls in classes if issubclass(cls, ObjectType))
