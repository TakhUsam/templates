import graphene
from graphene import ObjectType, List, String, Float
from graphene_django import DjangoObjectType

# THIS IMPORT IS REQUIRED
import graphql_geojson

from core.enums import UserRole
from core.models import User, Image, Message, Address
from app import settings


class ImageType(DjangoObjectType):

    url = graphene.String()

    def resolve_url(self, args):
        return f'{settings.MEDIA_URL}{self.url}'

    class Meta:
        model = Image
        fields = '__all__'
        use_connection = True


class UserType(DjangoObjectType):
    class Meta:
        model = User
        exclude = ('password',)
        use_connection = True

        order_fields = '__all__'
        search_fields = ['email', 'first_name', 'second_name', 'third_name']

    role = String()

    def resolve_role(self, args):
        return UserRole(self.role).name


class MessageType(DjangoObjectType):
    class Meta:
        model = Message
        fields = '__all__'
        use_connection = True


class AddressType(DjangoObjectType):
    class Meta:
        model = Address
        fields = '__all__'
        use_connection = True


class AddressSuggestionResultType(ObjectType):
    full_address = String()
    location = List(Float)
